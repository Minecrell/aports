# Contributor: Rudolf Polzer <divVerent@gmail.com>
# Maintainer: Rudolf Polzer <divVerent@gmail.com>
pkgname=aaaaxy
pkgver=1.2.321
pkgrel=1
pkgdesc="A nonlinear puzzle platformer taking place in impossible spaces"
url="https://divVerent.github.io/aaaaxy/"
arch="all !s390x !armhf !armv7 !riscv64"
license="Apache-2.0"
makedepends="
	alsa-lib-dev
	go
	libx11-dev
	libxcursor-dev
	libxinerama-dev
	libxi-dev
	libxrandr-dev
	mesa-dev
"
checkdepends="xvfb-run mesa-dri-gallium"
source="
	https://github.com/divVerent/aaaaxy/archive/v$pkgver/aaaaxy-$pkgver.tar.gz
	https://github.com/divVerent/aaaaxy/releases/download/v$pkgver/sdl-gamecontrollerdb-for-aaaaxy-v$pkgver.zip
"
options="net"  # Needed for go mod download.

export GOCACHE="$srcdir/go-cache"
export GOTMPDIR="$srcdir"
export GOMODCACHE="$srcdir/go"

prepare() {
	default_prepare

	mv "$srcdir"/third_party/SDL_GameControllerDB/assets/input/* \
		third_party/SDL_GameControllerDB/assets/input/
	go mod download
}

build() {
	export AAAAXY_BUILD_USE_VERSION_FILE=true
	make BUILDTYPE=release
}

check() {
	xvfb-run sh scripts/regression-test-demo.sh aaaaxy \
		"on track for Any%, All Paths and No Teleports" \
		./aaaaxy benchmark.dem
}

package() {
	install -Dm644 io.github.divverent.aaaaxy.metainfo.xml \
		"$pkgdir"/usr/share/metainfo/io.github.divverent.aaaaxy.metainfo.xml
	install -Dm644 aaaaxy.png \
		"$pkgdir"/usr/share/icons/hicolor/128x128/apps/aaaaxy.png
	install -Dm644 aaaaxy.desktop \
		"$pkgdir"/usr/share/applications/aaaaxy.desktop
	install -Dm755 aaaaxy \
		"$pkgdir"/usr/bin/aaaaxy
}

sha512sums="
967505d5cf25d09fc771f3c177413a0060a0d45a84737050ecc391fb77291073b44c9cd1e3f0de35997a10dd1301a43623c663181797175a00e2a82144e3df5f  aaaaxy-1.2.321.tar.gz
f48486748351a48c5dd490784d954c3596c1230e52c95a9218418d9663a124d674d0d67eb72ea9de63a959b9bbf9e83ddf392fd7186b95e4ffe169edf7366185  sdl-gamecontrollerdb-for-aaaaxy-v1.2.321.zip
"
